package org.actividad.lab2;


public class Kata_1 {
  public static void main(String[] args) {
    String reversedWord =  solution("word");
    System.out.println(reversedWord);
  }

  public static String solution(String str) {
    StringBuilder reversed = new StringBuilder();
    for (int i = str.length() - 1; i >= 0; i--) {
      reversed.append(str.charAt(i));
    }
    return reversed.toString();
  }
  }

