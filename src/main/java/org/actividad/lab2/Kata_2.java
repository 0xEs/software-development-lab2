package org.actividad.lab2;

public class Kata_2 {

  public static void main(String[] args) {
    String[] deckSteve = {"A", "7", "8"};
    String[] deckJosh = {"K", "5", "9"};

    String result = winner(deckSteve, deckJosh);
    System.out.println(result);
  }

  public static String winner(String[] deckSteve, String[] deckJosh) {
    int scoreSteve = 0;
    int scoreJosh = 0;

    // Mapa de valores numéricos de las cartas
    String cardValues = "23456789TJQKA";

    for (int i = 0; i < deckSteve.length; i++) {
      char cardSteve = deckSteve[i].charAt(0);
      char cardJosh = deckJosh[i].charAt(0);

      int valueSteve = cardValues.indexOf(cardSteve);
      int valueJosh = cardValues.indexOf(cardJosh);

      if (valueSteve > valueJosh) {
        scoreSteve++;
      } else if (valueSteve < valueJosh) {
        scoreJosh++;
      }
    }


    if (scoreSteve > scoreJosh) {
      return "Steve wins " + scoreSteve + " to " + scoreJosh;
    } else if (scoreSteve < scoreJosh) {
      return "Josh wins " + scoreJosh + " to " + scoreSteve;
    } else {
      return "Tie";
    }
  }
}
